using Microsoft.AspNetCore.Mvc;
using ProfileAPI.Models.DTO;
using ProfileAPI.Services.ControllerServices;
using ProfileAPI.Services.Extensions;
using System.Net.Mime;

namespace ProfileAPI.Controllers
{
    [ApiController]
    [Route("/api")]
    [Produces(MediaTypeNames.Application.Json)]
    public class LoginController : ControllerBase
    {
        private readonly LoginService _service;

        public LoginController(LoginService service)
        {
            _service = service;
        }

        [HttpPost("login")]
        public IResult Login(LoginDto request)
        {
            var result = _service.Authorization(request);

            return Response.ToJson(result);
        }

        [HttpPost("register")]
        public IResult Register(RegisterDto request)
        {
            var result = _service.Registration(request);

            return Response.ToJson(result);
        }
    }
}