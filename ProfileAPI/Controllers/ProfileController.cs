﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProfileAPI.Models.DTO;
using ProfileAPI.Services.ControllerServices;
using ProfileAPI.Services.Extensions;

namespace ProfileAPI.Controllers
{
    [ApiController]
    [Route("/api")]
    [Authorize]
    public class ProfileController : ControllerBase
    {
        private readonly ProfileService _serviceProfile;

        public ProfileController(ProfileService serviceProfile)
        {
            _serviceProfile = serviceProfile;
        }

        [HttpGet("me")]
        public IResult GetMyProfile()
        {
            var result = _serviceProfile.GetMyProfile(Request.Headers.Authorization);

            return Response.ToJson(result);
        }

        [HttpGet("user/{nickName}")]
        public IResult GetProfile(string nickName)
        {
            var result = _serviceProfile.GetProfile(nickName);

            return Response.ToJson(result);
        }

        [HttpPut("user/{nickname}")]
        public IResult UpdateProfile([FromRoute] string nickName, [FromForm] UserDto userDto)
        {
            var result = _serviceProfile.UpdateProfile(nickName, userDto, Request.Headers.Authorization);

            return Response.ToJson(result);
        }

        [HttpGet("user/{nickname}/template")]
        public IResult GetTemplate(string nickName)
        {
            var result = _serviceProfile.GetTemplate(nickName);

            return Response.ToJson(result);
        }

        [HttpPut("user/{nickname}/template")]
        public IResult UpdateTemplate(string nickName, List<TemplateElementDto> template)
        {
            var result = _serviceProfile.UpdateTemplate(nickName, template);

            return Response.ToJson(result);
        }
    }
}