﻿using System.Security.Cryptography;
using System.Text;

namespace ProfileAPI.Services.Extensions
{
    /// <summary>
    /// Расширение для криптографических функций
    /// </summary>
    public static class CryptographyExtension
    {
        /// <summary>
        /// Создание хеш строки, используя алгоритм SHA256
        /// </summary>
        /// <param name="value">строковое значение</param>
        /// <returns>Хеш, созданный алгоритмом SHA256</returns>
        public static string ToSHA256Value(this string value)
        {
            using (var sha256 = SHA256.Create())
            {
                var bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(value));

                return FormatterByteArray(bytes);
            }
        }

        /// <summary>
        /// Перевод байтов в строку
        /// </summary>
        /// <param name="data"></param>
        /// <returns>Строка, конвертированная из байтов</returns>
        private static string FormatterByteArray(byte[] data)
        {
            var stringBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                stringBuilder.Append($"{data[i]:x2}");
            }

            return stringBuilder.ToString();
        }

    }
}
