﻿using ProfileAPI.Models.DTO;

namespace ProfileAPI.Services.Extensions
{
    public static class ResponseExtension
    {
        private static readonly string _contentTypeJson = "application/json";

        public static IResult ToJson(this HttpResponse response, ResultDto data)
        {
            response.StatusCode = data.StatusCode;
            response.ContentType = _contentTypeJson;

            return Results.Json(data.Value, statusCode: data.StatusCode, contentType: _contentTypeJson);
        }
    }
}
