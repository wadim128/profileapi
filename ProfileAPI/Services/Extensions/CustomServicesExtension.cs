﻿using ProfileAPI.Services.ControllerServices;
using ProfileAPI.Services.Jwt;
using ProfileAPI.Services.Repository;
using ProfileAPI.Services.Repository.Interfaces;
using ProfileAPI.Services.SharedServices;

namespace ProfileAPI.Services.Extensions
{
    /// <summary>
    /// Добавление custom сервисов
    /// </summary>
    public static class CustomServicesExtension
    {
        /// <summary>
        /// Добавление scoped сервисов
        /// </summary>
        /// <param name="services">сервисы</param>
        /// <param name="configuration">конфигурация api</param>
        /// <returns>сервисы Api</returns>
        public static IServiceCollection AddScopedServices(this IServiceCollection services)
        {
            services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
            services.AddScoped<JwtService>();
            services.AddScoped<LoginService>();
            services.AddScoped<ProfileService>();
            services.AddScoped<FileService>();

            return services;
        }
    }
}
