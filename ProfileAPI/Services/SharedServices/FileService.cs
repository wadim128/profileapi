﻿using Newtonsoft.Json;
using ProfileAPI.Models.DTO;
using System.IO;
using System.Reflection;
using System.Text;

namespace ProfileAPI.Services.SharedServices
{
    public class FileService
    {
        private static readonly string _pathProject;
        private static readonly string _uploadsPath;
        private static readonly string _uploadsConstant;

        static FileService()
        {
            _uploadsConstant = "uploads";
            _pathProject = AppDomain.CurrentDomain.BaseDirectory;
            _uploadsPath = Path.Combine(_pathProject, _uploadsConstant);

            TryCreateDirectory(_uploadsPath);
        }

        /// <summary>
        /// Сохранение изображения
        /// </summary>
        /// <param name="name"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        public string? SaveImage(string name, IFormFile image)
        {
            var path = Path.Combine(_uploadsPath, name);
            var generateName = $"{name}_avatar{Path.GetExtension(image.FileName)}";
            var outputFileName = Path.Combine(path, generateName);

            TryCreateDirectory(path);

            if (File.Exists(outputFileName))
                File.Delete(outputFileName);

            try
            {
                using var stream = new FileStream(outputFileName, FileMode.Create, FileAccess.ReadWrite);

                image.CopyTo(stream);

                return Path.Combine(_uploadsConstant, name, generateName);
            }
            catch
            {
                return null;
            }


        }

        /// <summary>
        /// Сохранение нового шаблона
        /// </summary>
        /// <param name="name">имя папки</param>
        /// <param name="template">шаблон</param>
        public string SaveTemplate(string name, List<TemplateElementDto> template)
        {
            var path = Path.Combine(_uploadsPath, name);

            TryCreateDirectory(path);

            var templatePath = Directory.GetFiles(path, "*.json").FirstOrDefault();
            var generateName = $"{Guid.NewGuid()}.json";

            templatePath ??= Path.Combine(path, generateName);

            try
            {
                using var writer = new StreamWriter(templatePath, false, Encoding.UTF8);

                var jsonString = JsonConvert.SerializeObject(template);

                writer.WriteLine(jsonString);

                return Path.Combine(_uploadsConstant, name, generateName);
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Показ шаблона из JSON файла по имени папки
        /// </summary>
        /// <param name="name">название папки</param>
        /// <returns>Шаблон</returns>
        public List<TemplateElementDto>? GetTemplate(string name)
        {
            var path = Path.Combine(_uploadsPath, name);

            TryCreateDirectory(path);

            var templatePath = Directory.GetFiles(path, "*.json").FirstOrDefault();

            templatePath ??= Path.Combine(_uploadsPath, "default_template.json");

            var json = File.ReadAllText(templatePath);
            var result = JsonConvert.DeserializeObject<List<TemplateElementDto>>(json);

            return result;
        }

        /// <summary>
        /// Создание папки, если её не существует
        /// </summary>
        /// <param name="path">путь новой или текущей папки</param>
        /// <returns>True - если папка создана, или False - если была уже создана</returns>
        private static bool TryCreateDirectory(string path)
        {
            var creating = !Directory.Exists(path);

            if (creating)
                Directory.CreateDirectory(path);

            return creating;
        }
    }
}
