﻿using Microsoft.EntityFrameworkCore;

namespace ProfileAPI.Services.Database
{
    /// <summary>
    /// Расширение для слоя доступа к данным
    /// </summary>
    public static class DataAccessLayerExtension
    {
        /// <summary>
        /// Метод для подключения контекста данных
        /// </summary>
        /// <param name="services">сервисы</param>
        /// <param name="configuration">конфигурация</param>
        /// <returns>сервисы DAL</returns>
        public static IServiceCollection AddDalService(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationContext>(options =>
            {
                options.UseLazyLoadingProxies();
                options.UseSqlServer(configuration.GetConnectionString("Default")).LogTo(Console.WriteLine, LogLevel.Information); ;
            });
            services.AddDatabaseDeveloperPageExceptionFilter();

            return services;
        }

        /// <summary>
        /// Применение миграций к базе данных
        /// </summary>
        /// <param name="host"></param>
        public static void UseMigration(this IHost host)
        {
            using var scope = host.Services.CreateScope();

            var applicationContext = scope.ServiceProvider.GetService<ApplicationContext>();

            applicationContext?.Database.Migrate();
        }
    }
}
