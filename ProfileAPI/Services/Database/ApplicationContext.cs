﻿using Microsoft.EntityFrameworkCore;
using ProfileAPI.Models.DatabaseModels;

namespace ProfileAPI.Services.Database
{
    public class ApplicationContext : DbContext
    {
        DbSet<User> Users { get; set; } = null!;
        DbSet<UserInfo> UsersInfo { get; set; } = null!;

        public ApplicationContext(DbContextOptions<ApplicationContext> options) :base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<User>()
                .HasOne(a => a.UserInfo)
                .WithOne(a => a.User)
                .HasForeignKey<UserInfo>(c => c.UserId);
        }
    }
}
