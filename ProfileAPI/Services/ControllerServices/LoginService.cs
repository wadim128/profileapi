﻿using ProfileAPI.Models.DatabaseModels;
using ProfileAPI.Models.DTO;
using ProfileAPI.Services.Jwt;
using ProfileAPI.Services.Repository.Interfaces;

namespace ProfileAPI.Services.ControllerServices
{
    /// <summary>
    /// Сервис по работе с LoginController
    /// </summary>
    public class LoginService
    {
        private readonly IGenericRepository<User> _genericRepositoryUsers;
        private readonly JwtService _jwtService;
        public LoginService(IGenericRepository<User> genericRepositoryUsers, JwtService jwtService)
        {
            _genericRepositoryUsers = genericRepositoryUsers;
            _jwtService = jwtService;
        }

        /// <summary>
        /// Авторизация пользователя
        /// </summary>
        /// <param name="userData">данные по пользователю</param>
        /// <returns></returns>
        public ResultDto Authorization(LoginDto userData)
        {
            var user = _genericRepositoryUsers.Find(x => x.LoginName == userData.Login &&
                x.PasswordHash == userData.Password).FirstOrDefault();

            if (user is null)
            {
                return new ResultDto(StatusCodes.Status404NotFound, $"Пользователь {userData.Login} не найден в базе данных!");
            }

            return new ResultDto(StatusCodes.Status200OK, _jwtService.GenerateJwt(user));
        }

        /// <summary>
        /// Регистрация пользователя
        /// </summary>
        /// <param name="login">Логин пользователя</param>
        /// <param name="password">Пароль пользователя</param>
        /// <returns>Сообщение об успешности/не успешности добавленного пользователя</returns>
        public ResultDto Registration(RegisterDto userData)
        {
            var user = _genericRepositoryUsers.Find(x => x.LoginName == userData.Login &&
                x.PasswordHash == userData.Password).FirstOrDefault();

            if (user is not null)
                return new ResultDto(StatusCodes.Status409Conflict, $"Пользователь {userData.Login} уже зарегистрирован!");

            user = new User
            {
                NickName = userData.NickName,
                LoginName = userData.Login,
                PasswordHash = userData.Password,
            };

            try
            {
                _genericRepositoryUsers.Add(user);
            }
            catch(Exception ex)
            {
                return new ResultDto(StatusCodes.Status400BadRequest, ex.Message);
            }

            return new ResultDto(StatusCodes.Status200OK, $"Пользователь {userData.Login} успешно добавлен!");
        }
    }
}
