﻿using ProfileAPI.Models.DatabaseModels;
using ProfileAPI.Models.DTO;
using ProfileAPI.Services.Jwt;
using ProfileAPI.Services.Repository.Interfaces;
using ProfileAPI.Services.SharedServices;

namespace ProfileAPI.Services.ControllerServices
{
    /// <summary>
    /// Сервис по работе с ProfileController
    /// </summary>
    public class ProfileService
    {
        private readonly JwtService _jwtService;
        private readonly IGenericRepository<User> _genericRepositoryUser;
        private readonly FileService _fileService;

        public ProfileService(JwtService jwtService, IGenericRepository<User> genericRepositoryUser, FileService fileService)
        {
            _jwtService = jwtService;
            _genericRepositoryUser = genericRepositoryUser;
            _fileService = fileService;
        }

        /// <summary>
        /// Вывод собственного профиля
        /// </summary>
        /// <param name="token">jwt токен</param>
        /// <returns>Данные о пользователе</returns>
        public ResultDto GetMyProfile(string? authorized)
        {
            var isId = GetUserIdAuthorized(authorized, out Guid id);

            if (!isId)
                return new ResultDto(StatusCodes.Status404NotFound, "Идентификатор пользователя не найден!");

            var user = _genericRepositoryUser.GetById(id);

            return new ResultDto(StatusCodes.Status200OK, user);
        }

        /// <summary>
        /// Вывод профиля по никнейму пользователя
        /// </summary>
        /// <param name="nickName">никнейм пользователя</param>
        /// <returns>Данные пользователя или ошибка</returns>
        public ResultDto GetProfile(string nickName)
        {
            var user = _genericRepositoryUser.Find(x => x.NickName == nickName).FirstOrDefault();

            if (user is null)
                return new ResultDto(StatusCodes.Status404NotFound, $"Пользователь {nickName} не найден!");

            return new ResultDto(StatusCodes.Status200OK, user);
        }

        /// <summary>
        /// Обновление профиля пользователя
        /// </summary>
        /// <param name="nickName">никнейм пользователя</param>
        /// <param name="userData">данные пользователя</param>
        /// <returns>Результат запроса</returns>
        public ResultDto UpdateProfile(string nickName, UserDto userData, string? authorized)
        {
            var isNickName = GetUserNickNameAuthorized(authorized, out string nickNameAuthorized);

            if (!isNickName)
                return new ResultDto(StatusCodes.Status404NotFound, "Никнейм пользователя не найден!");

            if (nickName != nickNameAuthorized)
            {
                return new ResultDto(StatusCodes.Status409Conflict, $"Нельзя редактировать аккаунт {nickName}, так как это не ваш аккаунт!");
            }

            var user = _genericRepositoryUser.Find(x => x.NickName == nickName).FirstOrDefault();

            if (user is null)
                return new ResultDto(StatusCodes.Status404NotFound, $"Пользователь {nickName} не найден!");

            var path = userData.Avatar is null ? null : _fileService.SaveImage(nickName, userData.Avatar) ;

            if (user.UserInfo is null)
            {
                user.UserInfo = new UserInfo()
                {
                    DateBirth = userData.DateBirth,
                    Description = userData.Description,
                    Email = userData.Email,
                    FirstName = userData.FirstName,
                    LastName = userData.LastName,
                    Phone = userData.Phone,
                };

                if (path != null)
                {
                    user.UserInfo.Avatar = path;
                }
            }
            else
            {
                user.UserInfo.DateBirth = userData.DateBirth;
                user.UserInfo.Description = userData.Description;
                user.UserInfo.Email = userData.Email;
                user.UserInfo.FirstName = userData.FirstName;
                user.UserInfo.LastName = userData.LastName;
                user.UserInfo.Phone = userData.Phone;

                if (path != null)
                {
                    user.UserInfo.Avatar = path;
                }
            }

            _genericRepositoryUser.Update(user);

            return new ResultDto(StatusCodes.Status200OK, $"Данные пользователя {nickName} обновлены!");
        }

        /// <summary>
        /// Вывод GUID пользователя из header Authorization
        /// </summary>
        /// <param name="authorized">header авторизации</param>
        /// <param name="result">GUID пользователя</param>
        /// <returns>Если найден GUID то true, иначе false</returns>
        private bool GetUserIdAuthorized(string? authorized, out Guid result )
        {
            var jwt = _jwtService.GetJwtValue(authorized);

            if (jwt is null)
            {
                result = Guid.Empty;

                return false;
            }

            var isId = Guid.TryParse(_jwtService.ReadJwt(jwt)?.Claims.ToList()[0].Value, out result);

            return isId;
        }

        // <summary>
        /// Вывод GUID пользователя из header Authorization
        /// </summary>
        /// <param name="authorized">header авторизации</param>
        /// <param name="result">GUID пользователя</param>
        /// <returns>Если найден GUID то true, иначе false</returns>
        private bool GetUserNickNameAuthorized(string? authorized, out string result)
        {
            var jwt = _jwtService.GetJwtValue(authorized);

            if (jwt is null)
            {
                result = string.Empty;

                return false;
            }

            try
            {
                result = _jwtService.ReadJwt(jwt)?.Claims.ToList()[1].Value;

                return result != null && result != string.Empty;
            }
            catch
            {
                result = string.Empty;

                return false;
            }
        }

        public ResultDto UpdateTemplate(string nickName, List<TemplateElementDto> template)
        {
            var user = _genericRepositoryUser.Find(x => x.NickName == nickName).FirstOrDefault();

            if (user is null)
                return new ResultDto(StatusCodes.Status404NotFound, $"Пользователь {nickName} не найден!");

            if (template is null || template.Count == 0)
                return new ResultDto(StatusCodes.Status404NotFound, $"Пустой шаблон сохранить невозможно!");

            _fileService.SaveTemplate(nickName, template);

            return new ResultDto(StatusCodes.Status200OK, $"Шаблон пользователя {nickName} обновлен!");
        }

        public ResultDto GetTemplate(string nickName)
        {
            var user = _genericRepositoryUser.Find(x => x.NickName == nickName).FirstOrDefault();

            if (user is null)
                return new ResultDto(StatusCodes.Status404NotFound, $"Пользователь {nickName} не найден!");

            var result = _fileService.GetTemplate(nickName);

            return new ResultDto(StatusCodes.Status200OK, result);
        }
    }
}
