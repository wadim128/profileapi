﻿using Microsoft.IdentityModel.Tokens;
using ProfileAPI.Models.DatabaseModels;
using ProfileAPI.Models.DTO;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;

namespace ProfileAPI.Services.Jwt
{
    public class JwtService
    {
        private readonly IConfiguration _configuration;
        private readonly byte[] _secretKey;
        private int _tokenMinutes;

        public JwtService(IConfiguration configuration)
        {
            _configuration = configuration;
            _secretKey = Encoding.UTF8.GetBytes(_configuration["jwt:secretKey"] ?? "secretKey");
            _ = int.TryParse(_configuration["jwt:validTimeInMinutes"], out _tokenMinutes);
        }

        public TokenDto GenerateJwt(User userData)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, userData?.Id.ToString()),
                    new Claim(ClaimTypes.Name, userData?.NickName.ToString()),
                }),
                Expires = DateTime.Now.AddMinutes(_tokenMinutes),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(_secretKey), SecurityAlgorithms.HmacSha256)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            return new TokenDto()
            {
                Token = tokenHandler.WriteToken(token),
                NickName = userData.NickName,
                Expired = token.ValidTo
            };
        }

        public JwtSecurityToken? ReadJwt(string jwt)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            return tokenHandler.ReadJwtToken(jwt);
        }

        public string? GetJwtValue(string? authorizationHeader)
        {
            var isSuccess = AuthenticationHeaderValue.TryParse(authorizationHeader, out var headerValue);

            if (!isSuccess) return null;

            return headerValue?.Parameter ?? null;
        }
    }
}
