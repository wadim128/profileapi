﻿using System.ComponentModel.DataAnnotations;

public class MaxSizeAttribute : ValidationAttribute
{
    private readonly long _size = 512;

    public MaxSizeAttribute(long size)
    {
        _size = size;
    }

    protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
    {
        var file = value as IFormFile;

        if (file != null)
        {
            if (file.Length > _size)
            {
                return new ValidationResult(ErrorMessage);
            }
        }

        return ValidationResult.Success;
    }
}