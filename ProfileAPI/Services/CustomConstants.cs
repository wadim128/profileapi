﻿namespace ProfileAPI.Services
{
    public static class CustomConstants
    {
        public const string PATTERN_ONLY_LETTERS = @"^[a-zA-Zа-яА-Я]{1,}$";
    }
}
