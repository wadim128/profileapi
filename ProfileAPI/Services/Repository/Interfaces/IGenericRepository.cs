﻿using System.Linq.Expressions;

namespace ProfileAPI.Services.Repository.Interfaces
{
    /// <summary>
    /// Универсальный интерфейс доступа к данным
    /// </summary>
    /// <typeparam name="T">модель данных</typeparam>
    public interface IGenericRepository<T> where T : class
    {
        T? GetById(Guid id);
        IEnumerable<T> GetAll();
        IEnumerable<T> Find(Expression<Func<T, bool>> expression);
        void Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Remove(T entity);
        void RemoveRange(IEnumerable<T> entities);
        void Update(T entity);
    }
}
