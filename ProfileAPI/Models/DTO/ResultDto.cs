﻿namespace ProfileAPI.Models.DTO
{
    public class ResultDto
    {
        public ResultDto(int statusCode, object? value)
        {
            Value = value;
            StatusCode = statusCode;
        }

        public int StatusCode { get; set; } = StatusCodes.Status200OK;
        public object? Value { get; set; }
    }
}
