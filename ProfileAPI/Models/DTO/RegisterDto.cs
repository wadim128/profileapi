﻿using System.ComponentModel.DataAnnotations;

namespace ProfileAPI.Models.DTO
{
    public class RegisterDto : LoginDto
    {
        [Required]
        [MaxLength(50)]
        public string NickName { get; set; } = string.Empty;
    }
}
