﻿namespace ProfileAPI.Models.DTO
{
    public class PositionDto
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}
