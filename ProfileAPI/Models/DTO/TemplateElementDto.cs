﻿namespace ProfileAPI.Models.DTO
{
    public class TemplateElementDto
    {
        public List<PositionDto>? Index { get; set; }
        public string? Type { get; set; }
        public PositionDto? Size { get; set; }
        public string? Field { get; set; }
    }
}
