﻿using ProfileAPI.Services;
using System.ComponentModel.DataAnnotations;

namespace ProfileAPI.Models.DTO
{
    public class UserDto
    {
        [MaxLength(50)]
        [Required(ErrorMessage = "Поле firstName обязательно для заполнения!")]
        [RegularExpression(CustomConstants.PATTERN_ONLY_LETTERS)]
        public string FirstName { get; set; } = null!;

        [MaxLength(50)]
        [Required(ErrorMessage = "Поле lastName обязательно для заполнения!")]
        [RegularExpression(CustomConstants.PATTERN_ONLY_LETTERS)]
        public string LastName { get; set; } = null!;

        [MaxLength(255)]
        [EmailAddress]
        public string? Email { get; set; }

        [MaxLength(12)]
        [Phone]
        public string? Phone { get; set; }

        public DateTime DateBirth { get; set; }

        [MaxLength(255)]
        public string? Description { get; set; }

        [AllowedExtensions(new string[] { ".jpg", ".png", ".jpeg" }, ErrorMessage = "Неподдерживаемый тип изображения!" )]
        public IFormFile? Avatar { get; set; }
    }
}
