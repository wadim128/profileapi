﻿namespace ProfileAPI.Models.DTO
{
    public class TokenDto
    {
        public string Token { get; set; } = string.Empty;
        public string NickName { get; set; } = string.Empty;
        public DateTime? Expired { get; set; }
    }
}
