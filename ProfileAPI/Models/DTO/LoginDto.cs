﻿using System.ComponentModel.DataAnnotations;

namespace ProfileAPI.Models.DTO
{
    public class LoginDto
    {
        [Required]
        [MaxLength(10)]
        public string Login { get; set; } = string.Empty;
        [Required]
        public virtual string Password { get; set; } = string.Empty;
    }
}
