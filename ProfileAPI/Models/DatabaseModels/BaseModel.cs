﻿using System.Text.Json.Serialization;

namespace ProfileAPI.Models.DatabaseModels
{
    public class BaseModel
    {
        [JsonIgnore]
        public Guid Id { get; set; }
    }
}
