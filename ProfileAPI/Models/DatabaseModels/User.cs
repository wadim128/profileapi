﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace ProfileAPI.Models.DatabaseModels
{
    [Index(nameof(NickName), IsUnique = true)]
    [Index(nameof(LoginName), IsUnique = true)]
    public class User : BaseModel
    {
        #region Свойства
        [MaxLength(10)]
        public string LoginName { get; set; } = null!;

        [MaxLength(255)]
        [JsonIgnore]
        public string PasswordHash { get; set; } = null!;

        [MaxLength(50)]
        public string NickName { get; set; } = null!;
        #endregion

        #region Навигационные свойства
        public virtual UserInfo? UserInfo { get; set; }
        #endregion
    }
}
