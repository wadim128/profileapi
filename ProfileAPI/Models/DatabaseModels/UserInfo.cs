﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace ProfileAPI.Models.DatabaseModels
{
    public class UserInfo : BaseModel
    {
        #region Свойства
        [MaxLength(50)]
        public string FirstName { get; set; } = null!;

        [MaxLength(50)]
        public string LastName { get; set; } = null!;

        [MaxLength(255)]
        public string? Avatar { get; set; }

        [MaxLength(255)]
        public string? Email { get; set; }

        [MaxLength(12)]
        public string? Phone { get; set; }

        [Column(TypeName = "date")]
        public DateTime DateBirth { get; set; }

        [MaxLength(255)]
        public string? Description { get; set; }
        #endregion

        #region Навигационные свойства
        [JsonIgnore]
        public Guid UserId { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; } = null!;
        #endregion
    }
}
