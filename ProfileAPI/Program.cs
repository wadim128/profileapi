using Microsoft.Extensions.FileProviders;
using ProfileAPI.Services.Database;
using ProfileAPI.Services.Extensions;
using ProfileAPI.Services.Filters;
using ProfileAPI.Services.Jwt;

namespace ProfileAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(new WebApplicationOptions
            {
                Args = args,
                WebRootPath = AppDomain.CurrentDomain.BaseDirectory,
            });

            // Add services to the container.
            builder.Services.AddControllers(options =>
            {
                options.SuppressImplicitRequiredAttributeForNonNullableReferenceTypes = true;
                options.Filters.Add(new ValidateFilter());
            });
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            //Custom extensions
            builder.Services.AddDalService(builder.Configuration);
            builder.Services.AddScopedServices();

            //Authentication jwt
            builder.Services.AddJwtAuthentication(builder.Configuration);

            //CORS
            builder.Services.AddCors(policyBuilder =>
                policyBuilder.AddDefaultPolicy(policy =>
                policy.WithOrigins("*").AllowAnyHeader().AllowAnyMethod())
            );

            var app = builder.Build();


            app.UseSwagger();
            app.UseSwaggerUI();
            app.UseStaticFiles(new StaticFileOptions
            {
                RequestPath = "/api"
            });

            //App use services
            app.UseCors();
            app.UseHttpsRedirection();
            app.UseAuthentication();
            //Routing
            app.UseRouting();
            //--------------------------------------
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseMigration();

            Console.WriteLine($"WebRootPath: {builder.Environment.WebRootPath}");

            app.Run();
        }
    }
}